package com.example.dependencymodule;

import lombok.Data;

@Data
public class ClassNeededByDependentModule {
   public void print() {
        System.err.println("Hello, this is 'ClassNeededByDependentModule' speaking.");
    }
}
