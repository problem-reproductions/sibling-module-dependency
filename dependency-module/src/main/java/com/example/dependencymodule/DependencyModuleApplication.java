package com.example.dependencymodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DependencyModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DependencyModuleApplication.class, args);
    }

}
