package com.example.dependentmodule;

import com.example.dependencymodule.ClassNeededByDependentModule;
import jakarta.annotation.PostConstruct;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DependentModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DependentModuleApplication.class, args);
    }

    @PostConstruct
    void test() {
        ClassNeededByDependentModule temp = new ClassNeededByDependentModule();
        temp.print();
    }
}
